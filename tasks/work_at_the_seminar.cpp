﻿#include <iostream>
#include <list>
#include <string>
#include <math.h>
#include <algorithm>
#include <vector>
using namespace std;

// запись в список по разрядам
list<int> Push(list<int> vec,int data) 
{
	int b=1,c;
	while(b !=0)
	{
		b=data/10;
		c=data%10;
		vec.push_back(c);
		data=b;
	}
	return vec;
} 

// === Поиск минимального циклического числа х1,х2,х3,х4,х5,х6 ===
int Cyclic_Number()
{
	int x=0;
	list<int> tek;
	list<int> multi;
	int b=1, g, h, S;
	int A=2;

	for(A=1; A<1000000; A++)
	{
		x++;
		tek.clear();
		tek = Push(tek,A);
		int t = tek.back();
		S = tek.size();
		if(  t == 1)
		{
			if( (A%9) == 0 )
			{
				for(h=2; h!=7; h++)
				{
					multi.clear();
					multi = Push(multi,A*h);
					multi.sort();
					multi.unique();

					if(tek.size() != multi.size()) break;
					else
					{
						tek.sort();
						multi.sort();
						list<int>::iterator t = tek.begin();
						list<int>::iterator m = multi.begin();
						g=0;
						for(t = tek.begin(), m = multi.begin(); t!=tek.end(), m!=multi.end(); ++t, ++m)
						{
							if((*t) != (*m)) break;
							else g++;

							if( ( g == tek.size() ) && ( h == 6 ) ) return A; // 'A' и есть наша мечта

						}
					}
				}
			}
		}
		else
		{
			int s = tek.size();
			A = A+pow(10.0,s-1);
			A--;
		}
	}
}

void Print_Cyclic_Number()
{
	int Cycl = Cyclic_Number();
	cout << "Cyclic number : " << Cycl << endl;
	cout << "           x2 : " << 2*Cycl << endl;
	cout << "           x3 : " << 3*Cycl << endl;
	cout << "           x4 : " << 4*Cycl << endl;
	cout << "           x5 : " << 5*Cycl << endl;
	cout << "           x6 : " << 6*Cycl << endl;
}
//______________________________________________________________

// === Поиск числа удовл. условию a+b+c == 1000 и  a^2+b^2 == c^2 ===
int ABC()
{
	for(int a=1; a<1000; a++){
		for(int b=a+1; b<1000; b++){
			for(int c=b+1; c<1000; c++){
				if( ( (a+b+c) == 1000 )  &&  (a*a+b*b) == (c*c))
					return (a*b*c) ;
			}
		}
	}
}
//______________________________________________________________

// === Сумма 2-х чисел и разность пентагональное число === 
int Pentag(int n ){
	return n*(3*n-1)/2;
}

int Pentagonal_Number()
{
	long int A[5000];
	for (int i=0;i<5000;i++){
		A[i]=Pentag(i);

	}
	for (int i=1000; i<5000; i++){
		for (int j=i+1; j<5000; j++){
			for (int s=j; s<5000; s++){

				if (A[i]+A[j]==A[s])
				{
					for (int k=1;k<5000;k++){

						if(A[j]-A[i]==A[k])
							return A[k];
					}
				}
			}
		}
	}
}

int main(){
	cout << ABC() << endl;
	Print_Cyclic_Number();
	cout << "\nPentagonal Number: " <<Pentagonal_Number() << endl;
	system("pause");
	return 0;
}

