#include <iostream>
#include <list>
#include <string>
using namespace std;

class Car{
protected:
	int engine;
	int tyresSize;
public:
	Car(){};
	Car(int _engine, int _tyresSize){};
	int get_engine(){ return engine; };
	int get_tyresSize(){ return tyresSize; };
	virtual string Vendor() = 0;
	virtual ~Car(){ cout << "~Car delete" << endl;};
};

class BMW :public Car{
public:
	BMW(){};
	BMW(int _engine, int _tyresSize)
	{
		engine = _engine;
		tyresSize = _tyresSize;
	};
	string Vendor(){ return "BMW:"; }
	~BMW(){cout << "~Bmw delete " << endl;};
};

class AUDI :public Car{
public:
	AUDI(){};
	AUDI(int _engine, int _tyresSize)
	{
		engine = _engine;
		tyresSize = _tyresSize;
	};
	string Vendor(){ return "AUDI: "; }
	~AUDI(){cout << "~Audi delete " << endl;};
};


int main(){
	int n;
	int _engine, _tyredSize;
	list<Car*> ls;
	list<Car*>::iterator p = ls.begin();
	list<Car*>::iterator a = ls.begin();
	while (true)
	{
		cout << endl;
		cout << " --MENU---" << endl;
		cout << "1) Create a car." << endl;
		cout << "2) Delete cars." << endl;
		cout << "3) View cars." << endl;
		cout << "4) Exit." << endl;
		cin >>n;
		if (n == 1)
		{
			system ("cls");
			cout << "What kind of car to create?" << endl;
			cout << "1) Bmw." << endl;
			cout << "2) Audi." << endl;
			cin >>n;
			if (n == 1)
			{
				cout << "Enter the volume of the engine: ";
				cin >>_engine;
				cout << "Enter the tire size: ";
				cin >>_tyredSize;
				ls.push_back(new BMW(_engine, _tyredSize));
				cout << "BMW created." << endl;
			}
			else if (n == 2)
			{
				cout << "Enter the volume of the engine: ";
				cin >>_engine;
				cout << "Enter the tire size: ";
				cin >>_tyredSize;
				ls.push_back(new AUDI(_engine, _tyredSize));
				cout << "AUDI created." << endl;
			}
		}
		else if (n == 2)
		{
			int b=1;
			int N=1;
			p = ls.begin();
			system ("cls");
			if( ls.size() == 0 ) cout << "No cars! Create a car." << endl;
			else
			{
				cout << "Enter the number of car what you want delete." << endl;
				for (a = ls.begin(); a != ls.end(); a++)
				{
					cout << N << ") " << (*a)->Vendor()  << endl;
					cout << "   Engine:" << (*a)->get_engine() << endl;
					cout << "   TyresSize:" << (*a)->get_tyresSize() << endl << endl;
					N++;
				}
				cin >> n;
				while(b != n)
				{
					b++;
					p++;
				}
				(*p)->~Car();
				ls.remove((*p));
			}
		}
		else if (n == 3)
		{
			system ("cls");
			int N = 1;
			if( ls.size() == 0 ) cout << "No cars! Create a car." << endl;
			else
			{
				for (a = ls.begin(); a != ls.end(); a++)
				{
					cout << N << ") " << (*a)->Vendor()  << endl;
					cout << "   Engine:" << (*a)->get_engine() << endl;
					cout << "   TyresSize:" << (*a)->get_tyresSize() << endl << endl;
					N++;
				}
			}
		}
		else if (n == 4)
		{
			for (a = ls.begin(); a != ls.end(); a++) (*a)->~Car();
			break;
		}
	}

	system("pause");
	return 0;
}