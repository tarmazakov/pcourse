#include <iostream>
using namespace std;
class A{
public:
	A* left;
	A* right;
	int data;
};
int Add (A* root, int a)
{
	if(root->left == 0)
	{	
		root->left = new A();
		root->left->left = 0;
		root->left->right = 0;
		root->left->data = a;
	}
	else 
	{
		A* current;
		current = root->left;
		while(1)
		{
				if (a < current->data)
				{
					if (current->left == NULL) 
					{
						current->left = new A();
						current->left->left = 0;
						current->left->right = 0;
						current->left->data = a;
						return 0;
					}
					else current = current->left;		
				}
				else
					{
						if (current->right == NULL)
						{
							current->right = new A();
							current->right->left = 0;
							current->right->right = 0;
							current->right->data = a;
							return 0;
						}
						else current = current->right;
					}							
			}
		
}

	return 0;
}
int Print(A* root){
	if(root->left != NULL) Print(root->left); 
	cout<<root->data << endl;
	if(root->right != NULL) Print(root->right); 
	return 0;
}
int main(){
	int n,a;
	A* root = new A();
	//root->data = 0;
	cout << "Enter the number of elements you want to add a binary tree: " ;
	cin >> n ;
	cout << endl << "Enter the elements: ";
	for (int i=0; i<n; i++)
	{
		cin >> a;
		Add(root,a);
	}
	cout << endl;
	Print (root->left);
	return 0;
}