#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
using namespace std;

int value (const string& _str)
{
	int sum = 0;
	for(string::const_iterator it = _str.begin(); it != _str.end(); ++it )
	{
		sum +=(int) (*it - 'A' +1);
	}
	return sum;
}

int main()
{
	vector<string> vec;
	ifstream in("text.txt");
	string line;
	while(getline(in, line, ','))
	{
		line.erase(0,1);
		line.erase(line.size()-1,1);
		vec.push_back(line);
	}
	sort(vec.begin(), vec.end());
	int SUM= 0, I=1, n;

	for(vector<string>::iterator a = vec.begin(); a != vec.end(); ++a )
	{
		n = value(*a)*I;
		I++;
		SUM += n;
	}
	cout << SUM;
	system("pause");
	return 0;
}
