// Балансировка дерева
#include <iostream>
using namespace std;

class A {
public:
	int data;
	int balance;
	A* left;
	A* right;
};

int balance( A* tek ) {
	if( tek == NULL )
		return -1;
	else
		return tek->balance;
}

int Max(int a, int b){return a > b ? a : b;}

A* RotateLeft( A* R ) {
	A* T1;
	T1 = R->left;
	R->left = T1->right;
	T1->right = R;
	R->balance = Max(balance(R->left), balance(R->right)) + 1;
	T1->balance = Max( balance( T1->left ), R->balance ) + 1;
	return T1;  
}


A* RotateRight( A* R ) {
	A* T1;
	T1 = R->right;
	R->right = T1->left;
	T1->left = R;
	R->balance = Max(balance(R->left), balance(R->right)) + 1;
	T1->balance = Max( balance( T1->right ), R->balance ) + 1;
	return T1;  
}


A* DoubleRotateLeft( A* K ) {
	K->left = RotateRight( K->left );
	return RotateLeft( K );
}


A* DoubleRotateRight( A* K ) {
	K->right = RotateLeft( K->right );
	return RotateRight( K );
}

A* Add(A* tek, int a){
	if( tek == NULL ){
		tek = new A();
		tek->data = a; 
		tek->balance = 0;
		tek->left = tek->right = NULL;

	}
	else if( a < tek->data ) {
		tek->left = Add(tek->left, a);
		if( balance( tek->left ) - balance( tek->right ) == 2 )
			if( a < tek->left->data )
				tek = RotateLeft( tek );
			else
				tek = DoubleRotateLeft( tek );
	}
	else if( a >= tek->data ) {
		tek->right = Add(tek->right, a);
		if( balance( tek->right ) - balance( tek->left ) == 2 )
			if( a > tek->right->data )
				tek = RotateRight( tek );
			else
				tek = DoubleRotateRight( tek );
	}
	tek->balance = Max(balance(tek->left), balance(tek->right)) + 1;
	return tek;
}


void Print(A* root, int n)
{
	if ( root != NULL ) 
	{
		Print(root->right, n+1);
		for (int i=0; i < n; i++) cout << "   ";
		cout << root->data;
		Print(root->left, n+1);
	}
	else cout << endl;
}


int main(){
	int info,n,c=1;
	A* head = new A();
	A* root;
	root = head->left;
	while(c != 0)
	{	
		cout << endl;
		cout << "---MENU---" << endl;
		cout << "1. Add the element. " << endl;
		cout << "2. Print tree." << endl;
		cin >> n;
		if(n == 1)
		{
			system("cls");
			cout << "Enter the element:" << endl;
			cin >> info;
			root = Add(root,info);
		}
		else if( n == 2) 
		{
			system("cls");
			cout << "---TREE---(the root of the left, see the tree from left to right)" << endl;
			Print(root,0);
		}
	}
	system("pause");
	return 0;
}

